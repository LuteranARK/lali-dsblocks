#ifndef UTIL_H
#define UTIL_H

#include "shared.h"

#if 0
#define COL0                            "\x0b"	/* default status color */
#define COL1                            "\x0c"	/* default icon color */
#define COL2                            "\x0d"	/* alternate icon color */
#define COL3                            "\x0e"	/* mail block - syncing */
#define COL4                            "\x0f"	/* mail block - frozen */
#else
#define COL0                            ""
#define COL1                            ""
#define COL2                            ""
#define COL3                            ""
#define COL4                            ""
#endif

#define SCRIPT(name)                    "/home/luteran/.local/bin/blocks/"name
#define TERMCMD(...)                    spawn((const char *const []){ "st", __VA_ARGS__, NULL })

#define SPRINTF(str, ...)               snprintf(str, BLOCKLENGTH, __VA_ARGS__)

size_t getcmdout(const char *const *arg, char *cmdout, size_t cmdoutlen);
int readv(const char *path, const char *restrict fmt, ...) __attribute__ ((format(scanf, 2, 3)));
pid_t spawn(const char *const *arg);

#endif  // UTIL_H
