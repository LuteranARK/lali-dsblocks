#ifndef SHARED_H
#define SHARED_H

#include <limits.h>
#include <X11/Xlib.h>

#define BLOCKLENGTH                     4096
#define NILL                            INT_MIN
#define NO_U                            INT_MAX

#define LENGTH(X)                       (sizeof X / sizeof X[0])

void cleanup(void);

#endif  // SHARED_H
