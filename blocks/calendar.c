#include <time.h>
#include <locale.h>

#include "../util.h"
#include "calendar.h"

// #define ICON                    COL1 "" COL0

void
calendaru(char *str, int sigval)
{
    setlocale(LC_ALL, "hu_HU.UTF-8");

	time_t t = time(NULL);

	strftime(str, BLOCKLENGTH, "| %A, %b. %d [%R]", localtime(&t));
}

int
calendarc(int button)
{
	if (button <= 3)
		TERMCMD("calcurse");
	return NO_U;
}
