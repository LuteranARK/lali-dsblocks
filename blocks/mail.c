#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>

#include "../util.h"
#include "mail.h"

#define ICONn                           COL1 "" COL0	/* last sync successful */
#define ICONe                           COL2 "" COL0	/* last sync failed */
#define ICONs                           COL3 "" COL0	/* syncing */
#define ICONz                           COL4 "" COL0	/* frozen */

#define MAILSYNC                        (const char *const []){ SCRIPT("mailsync.sh"), NULL }

static const char newmaildir[] = "/home/nacho/.local/share/mail/iiser/INBOX/new";
static int newmails;

static void
updatenewmails(void)
{
	static time_t lastmtime = 0;
	DIR *dir;
	struct dirent *entry;
	struct stat buf;

	if (stat(newmaildir, &buf) == -1) {
		newmails = -1;
		return;
	}
	if (buf.st_mtime == lastmtime)
		return;
	lastmtime = buf.st_mtime;

	if (!(dir = opendir(newmaildir))) {
		newmails = -1;
		return;
	}
	newmails = 0;
	while ((entry = readdir(dir)))
		if (entry->d_type == DT_REG)
			newmails++;
	closedir(dir);
}

void
mailu(char *str, int sigval)
{
	static int frozen;

	/* update newmails on signals from MAILSYNC */
	if (sigval >= -1) {
		updatenewmails();
		if (newmails < 0) {
			*str = '\0';
			return;
		}
	}
	switch (sigval) {
		/* routine update */
	case NILL:
		if (!frozen)
			spawn(MAILSYNC);
		break;
		/* toggle frozen */
	case -2:
		if (newmails < 0)
			break;
		if (frozen) {
			spawn(MAILSYNC);
		} else {
			frozen = 1;
			SPRINTF(str, ICONz "%d", newmails);
		}
		break;
		/* MAILSYNC started */
	case -1:
		frozen = 0;
		SPRINTF(str, ICONs "%d", newmails);
		break;
		/* sync successful */
	case 0:
		if (frozen)
			SPRINTF(str, ICONz "%d", newmails);
		else
			SPRINTF(str, ICONn "%d", newmails);
		break;
		/* sync failed */
	default:
		if (frozen)
			SPRINTF(str, ICONz "%d", newmails);
		else
			SPRINTF(str, ICONe "%d", newmails);
	}
}

int
mailc(int button)
{
	if (button == 3)
		return -2;
	if (button == 1)
		spawn(MAILSYNC);

	return NO_U;
}
