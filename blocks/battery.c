#include <stdio.h>
#include <string.h>

#include "../util.h"
#include "battery.h"

#define ICONe                           COL2 "" COL0	/* unexpected error */
#define ICONa                           COL1 "" COL0	/* no battery */

#define PDN                             COL1
#define PUP                             COL2

#define ICON0                           "" COL0
#define ICON1                           "" COL0
#define ICON2                           "" COL0
#define ICON3                           "" COL0
#define ICON4                           "" COL0
#define ICON5                           "" COL0
#define ICON6                           "" COL0
#define ICON7                           "" COL0
#define ICON8                           "" COL0
#define ICON9                           "" COL0

#define ICON(bat)                       icons[(bat * (LENGTH(icons) - 1) + 50) / 100]

#define BATC                            10	/* critical level */
#define BATL                            20	/* low level */
#define BATP                            40	/* plug in level */
#define BATU                            101	/* unplug level */

#define BATCAPFILE                      "/sys/class/power_supply/BAT1/capacity"
#define ACSTATEFILE                     "/sys/class/power_supply/ACAD/online"
#define BATCFULLFILE                    "/sys/class/power_supply/BAT1/charge_full"
#define BATCNOWFILE                     "/sys/class/power_supply/BAT1/charge_now"
#define BATRATEFILE                     "/sys/class/power_supply/BAT1/current_now"

#define CNOTIFY(t, msg)                (const char *const []){ "notify-send", \
                                                   "-h", "string:x-canonical-private-synchronous:batmon", \
                                                   "-u", "critical", \
                                                   "-t", t, \
                                                   "BatMon", msg, NULL }

#define NNOTIFY(t, msg)                (const char *const []){ "notify-send", \
                                                   "-h", "string:x-canonical-private-synchronous:batmon", \
                                                   "-t", t, \
                                                   "BatMon", msg, NULL }

#define TNOTIFY(t, msg)                (const char *const []){ "notify-send", \
                                                   "-h", "string:x-canonical-private-synchronous:batmon", \
                                                   "-h", "int:transient:1", \
                                                   "-t", t, \
                                                   "BatMon", msg, NULL }

enum { Normal, Critical, Low, Plug, Unplug };

void
batteryu(char *str, int ac)
{
	static int level = Normal;
	static const char *const icons[] = {
		ICON0, ICON1, ICON2, ICON3, ICON4,
		ICON5, ICON6, ICON7, ICON8, ICON9,
	};
	int bat;

	if (!readv(BATCAPFILE, "%d", &bat)) {
		strcpy(str, ICONa);
		return;
	}
	/* routine update */
	if (ac == NILL) {
		if (!readv(ACSTATEFILE, "%d", &ac)) {
			SPRINTF(str, ICONe "%d%%", bat);
			return;
		}
		if (ac) {
			if (bat < BATU)
				level = Normal;
			else {
				if (level != Unplug) {
					spawn(NNOTIFY
					      ("0", "Teljesen feltöltve!"));
					level = Unplug;
				}
			}
            if (bat <= 95) {
			    SPRINTF(str, PUP " BAT: %d%% |", bat);
            } else {
			    SPRINTF(str, PUP " BAT: %s%% |", "100");
            }
		} else {
			if (bat > BATP)
				level = Normal;
			else if (bat > BATL) {
				if (level != Plug) {
					spawn(NNOTIFY
					      ("0", "Csatlakoztassa a töltőt!"));
					level = Plug;
				}
			} else if (bat > BATC) {
				if (level != Low) {
					spawn(NNOTIFY
					      ("0", "Az akkumulátor töltöttségi szintje alacsony!"));
					level = Low;
				}
			} else {
				if (level != Critical) {
					spawn(CNOTIFY
					      ("0", "Az akkumulátor töltöttségi szintje kritikus!"));
					level = Critical;
				}
			}
			SPRINTF(str, PDN " BAT: %s%d%% |", ICON(bat), bat);
		}
		/* charger plugged in */
	} else if (ac) {
		if (bat < BATU) {
			spawn(TNOTIFY("1000", "Töltő csatlakoztatva"));
			level = Normal;
		} else {
			spawn(NNOTIFY("0", "Húzza ki a töltőt!"));
			level = Unplug;
		}
		SPRINTF(str, PUP " BAT: %s%d%% |", ICON(bat), bat);
		/* charger plugged out */
	} else {
		if (bat > BATP) {
			spawn(TNOTIFY("1000", "A töltő ki van húzva!"));
			level = Normal;
		} else if (bat > BATL) {
			spawn(NNOTIFY("0", "Csatlakoztassa a töltőt"));
			level = Plug;
		} else if (bat > BATC) {
			spawn(NNOTIFY("0", "Az akkumulátor töltöttségi szintje alacsony!"));
			level = Low;
		} else {
			spawn(CNOTIFY("0", "Az akkumulátor töltöttségi szintje kritikus!"));
			level = Critical;
		}
		SPRINTF(str, PDN " BAT: %s%d%% |", ICON(bat), bat);
	}
}

int
batteryc(int button)
{
	int ac, cur, cnow, rate, hr, mn;
	char buf[64];

	if (!readv(ACSTATEFILE, "%d", &ac)) {
		spawn(CNOTIFY("0", "Error: couldn't read " ACSTATEFILE));
		return NO_U;
	}
	if (ac) {
		if (!readv(BATCFULLFILE, "%d", &cur) ||
		    !readv(BATCNOWFILE, "%d", &cnow) ||
		    !readv(BATRATEFILE, "%d", &rate)) {
			spawn(NNOTIFY("1000", "On AC power"));
			return NO_U;
		}
		cur -= cnow;
	} else {
		if (!readv(BATCNOWFILE, "%d", &cur)) {
			spawn(CNOTIFY
			      ("0", "Error: couldn't read " BATCNOWFILE));
			return NO_U;
		}
		if (!readv(BATRATEFILE, "%d", &rate)) {
			spawn(CNOTIFY
			      ("0", "Error: couldn't read " BATRATEFILE));
			return NO_U;
		}
	}
	if (rate == 0 || cur == 0) {
		spawn(NNOTIFY("2000", "Battery fully charged"));
		return NO_U;
	}

	mn = (cur * 60 + 59) / rate;
	hr = mn / 60;
	mn = mn % 60;

	snprintf(buf, sizeof buf, "%dh %dm remaining", hr, mn);
	spawn(NNOTIFY("2000", buf));

	return NO_U;
}
