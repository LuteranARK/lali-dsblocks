#include <stdio.h>

#include "../util.h"
#include "vol1.h"

// #define ICONsn                          COL1 "" COL0
// #define ICONsm                          COL2 "" COL0
// #define ICONhn                          COL1 "" COL0
// #define ICONhm                          COL2 "" COL0

#define PULSEINFO                       (char *[]){ SCRIPT("vol1.sh"), NULL }

// #define PAVUCONTROL                     (char *[]){ "pavucontrol-qt", NULL }
// #define NORMALIZEVOLUME                 (char *[]){ SCRIPT("pulse_normalize.sh"), NULL }
// #define TOGGLEMUTE                      (char *[]){ "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL }

size_t
volume1u(char *str, int sigval)
{
        // static char *icons[] = { ICONsn, ICONsm, ICONhn, ICONhm };
        char buf[32];
        size_t l;

        if (!(l = getcmdout(PULSEINFO, buf, sizeof buf - 1))) {
                *str = '\0';
                return 1;
        }
        buf[l] = '\0';
        return SPRINTF(str, " VOL: %s ", buf);
}
