#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "../util.h"
#include "ram.h"

#define RAMFILE                         "/proc/meminfo"

#define MEMTOTAL                        "MemTotal:"
#define MEMAVAIL                        "MemAvailable:"
#define LASTFIELDTOSCAN                 "Dirty:"

size_t
ramu(char *str, int sigval)
{
        FILE *fp;
        char field[32];
        uintmax_t val, memavail = 0, memtotal = 0;
        unsigned int memu;

        if (!(fp = fopen(RAMFILE, "r"))) {
                *str = '\0';
                return 1;
        }
        do {
                if (fscanf(fp, "%s %ju kB", field, &val) != 2) {
                        fclose(fp);
                        *str = '\0';
                        return 1;
                }
                if (!memtotal && strncmp(field, MEMTOTAL, sizeof MEMTOTAL) == 0) {
                        memtotal = val;
                        continue;
                } else if (!memavail && strncmp(field, MEMAVAIL, sizeof MEMAVAIL) == 0) {
                        memavail = val;
                        break;
                }
        } while (strncmp(field, LASTFIELDTOSCAN, sizeof LASTFIELDTOSCAN) != 0);
        fclose(fp);

        if (!memtotal) {
                *str = '\0';
                return 1;
        }

        memu = memtotal - memavail;
        return SPRINTF(str, "| MEM: %uM |", memu / 1024);
}
