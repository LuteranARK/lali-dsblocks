#include <stdio.h>
#include <stdlib.h>

#include "../util.h"
#include "wet.h"

static const char wthfile[] = "/home/luteran/.local/share/idojaras.txt";

void
wetu(char *str, int sigval)
{
    FILE* ptr;
    char c[64];

    // Opening file in reading mode
    ptr = fopen(wthfile, "r");

    if (NULL == ptr) {
        printf("file can't be opened \n");
    }

    while (fgets(c, 64, ptr) != NULL) {
        SPRINTF(str, "| %s ", c);
    }

    // Closing the file
    fclose(ptr);
}

int
wetc(int button)
{
    if (button <= 3)
        TERMCMD("calcurse");
    return NO_U;
}
