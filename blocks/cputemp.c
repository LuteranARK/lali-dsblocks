#include <stdio.h>

#include "../util.h"
#include "cputemp.h"

// #define ICONl                           COL1 "" COL0
// #define ICONm                           COL2 "" COL0
// #define ICONh                           COL3 "" COL0

// #define TEMPl                           30000
// #define TEMPh                           60000

static const char cputempfile[] = "/sys/class/thermal/thermal_zone2/temp";

void
cputempu(char *str, int sigval)
{
	size_t temp;

	if (!readv(cputempfile, "%zu", &temp)) {
		*str = '\0';
		return;
	}

	// if (temp < TEMPl)
	// 	SPRINTF(str, ICONl " %zuºC", temp / 1000);
	// else if (temp < TEMPh)
	// 	SPRINTF(str, ICONm " %zuºC", temp / 1000);
	// else
	SPRINTF(str, "temp: %zu°C ", temp / 1000);
}

// int
// cputempc(int button)
// {
// 	switch (button) {
// 	case 1:
// 		TERMCMD("htop", "-s", "PERCENT_CPU");
// 		break;
// 	case 2:
// 		TERMCMD("htop");
// 		break;
// 	case 3:
// 		TERMCMD("htop", "-s", "PERCENT_MEM");
// 		break;
// 	}
// 	return NO_U;
// }
