#include <stdio.h>

#include "../util.h"
#include "nettraf.h"

// #define ICONdn ""
// #define ICONup ""

static const char receivefile[] = "/sys/class/net/wlan0/statistics/rx_bytes";
static const char transmitfile[] = "/sys/class/net/wlan0/statistics/tx_bytes";

void
nettrafu(char *str, int sigval)
{
	static const char *const suffix[] = {
		"B",
		"Ki",
		"Mi",
		"Gi",
	};

	static size_t last_received, last_transmitted;
	size_t diff_received, diff_transmitted;
	size_t i, j;

	diff_received = last_received;
	diff_transmitted = last_transmitted;
	readv(receivefile, "%zu", &last_received);
	readv(transmitfile, "%zu", &last_transmitted);

	diff_received = last_received - diff_received;
	diff_transmitted = last_transmitted - diff_transmitted;

	i = j = 0;
	while (diff_received >= 1024) {
		++i;
		diff_received /= 1024;
	}
	while (diff_transmitted >= 1024) {
		++j;
		diff_transmitted /= 1024;
	}

	SPRINTF(str, " LE: %4zu%s " "FEL: %4zu%s |", diff_received, suffix[i],
		diff_transmitted, suffix[j]);
}
