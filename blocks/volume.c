#include <stdio.h>

#include "../util.h"
#include "volume.h"

static int defaultsource;

void
volumeu(char *str, int sigval)
{
    static const char *const action[] = { "-t", "-i1", "-d1" };
    char buf[32];
    size_t l;
    const char *const cmd[6] = { "pamixer", "--get-mute", "--get-volume",
        defaultsource ? "--default-source" : "",
        (unsigned int) sigval < LENGTH(action) ? action[sigval] : NULL
    };
    int mute, vol;

    if (!(l = getcmdout(cmd, buf, sizeof buf - 1)))
        return;

    buf[l] = '\0';
    sscanf(buf, "%*s%n %d", &mute, &vol);

    if (mute == sizeof "true" - 1)
        SPRINTF(str, " VOL: OFF ");
    else
        SPRINTF(str, " VOL: %d%% ", vol);
}

int
volumec(int button)
{
    switch (button) {
        case 1:
            defaultsource ^= 1;
        break;
        case 2:
            TERMCMD("pulsemixer");
            return NO_U;
        case 3:
            return 0;
        case 4:
            return 1;
        case 5:
            return 2;
    }
    return NILL;
}
