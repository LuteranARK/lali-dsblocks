#include <string.h>

#include "../util.h"
#include "hotspot.h"

#define ICON                            COL1 ""

#define TURNOFFHOTSPOT                  (const char *const []){ "doas", "-n", SCRIPT("hotspot.sh"), "terminate", NULL }

void
hotspotu(char *str, int enabled)
{
	if (enabled)
		strcpy(str, ICON);
	else
		*str = '\0';
}

int
hotspotc(int button)
{
	spawn(TURNOFFHOTSPOT);
	return 0;
}
