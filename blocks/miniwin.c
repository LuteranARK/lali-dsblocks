#include <stdio.h>

#include "../util.h"
#include "miniwin.h"

#define MINIW                           (char *[]){ SCRIPT("minitest.sh"), NULL }

size_t
miniwinu(char *str, int sigval)
{
        char buf[32];
        size_t l;

        if (!(l = getcmdout(MINIW, buf, sizeof buf - 1))) {
                *str = '\0';
                return 1;
        }
        buf[l] = '\0';
        return SPRINTF(str, "%s", buf);
}
