#include <stdio.h>
#include <string.h>

#include "../util.h"
#include "cpuuse.h"

static const char statfile[] = "/proc/stat";

void
cpuuseu(char *str, int sigval)
{
    static long double a[10];
    long double b[10], sum;
    int cpu_u;

    memcpy(b, a, sizeof(b));
    /* cpu user nice system idle iowait irq softirq */
    if (readv(statfile, "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
              &a[0], &a[1], &a[2], &a[3], &a[4], &a[5], &a[6], &a[7], &a[8], &a[9])
    != 10) {
        return;
    }

    if (b[0] == 0.0) {
        return;
    }

    sum = (b[0] + b[1] + b[2] + b[3] + b[4] + b[5] + b[6] +b[7] + b[8] + b[9]) -
        (a[0] + a[1] + a[2] + a[3] + a[4] + a[5] + a[6] + a[7] + a[8] + a[9]);

    if (sum == 0.0) {
        return;
    }

    cpu_u = (100.0 * ((b[0] + b[1] + b[2] + b[5] + b[6] + b[7] + b[8] + b[9]) - (a[0] + a[1] + a[2] + a[5] + a[6] + a[7] + a[8] + a[9])) / sum);

    SPRINTF(str, "%d%% CPU, ", cpu_u );
}

int
cpuusec(int button)
{
    switch (button) {
        case 1:
            TERMCMD("htop", "-s", "PERCENT_CPU");
            break;
        case 2:
            TERMCMD("htop");
            break;
        case 3:
            TERMCMD("htop", "-s", "PERCENT_MEM");
            break;
    }
    return NO_U;
}
