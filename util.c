#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "util.h"

/* getcmdout doesn't null terminate cmdout
 * make sure that terminal newline character is handled if the command spits one */
size_t
getcmdout(const char *const *arg, char *cmdout, size_t cmdoutlen)
{
	int fd[2];
	size_t trd;
	ssize_t rd;

	if (pipe(fd) == -1) {
		perror("getcmdout - pipe");
		cleanup();
		exit(1);
	}
	switch (fork()) {
        case -1:
            perror("getcmdout - fork");
            close(fd[0]);
            close(fd[1]);
            cleanup();
            exit(1);
        case 0:
            close(fd[0]);
            if (fd[1] != STDOUT_FILENO) {
                if (dup2(fd[1], STDOUT_FILENO) != STDOUT_FILENO) {
                    perror("getcmdout - child - dup2");
                    close(fd[1]);
                    exit(1);
                }
                close(fd[1]);
            }
            execvp(arg[0], (char *const *) arg);
            perror("getcmdout - child - execvp");
            _exit(127);
        default:
            close(fd[1]);
            trd = 0;
            do
                rd = read(fd[0], cmdout + trd, cmdoutlen - trd);
            while (rd > 0 && (trd += rd) < cmdoutlen);
            close(fd[0]);
            if (rd == -1) {
                perror("getcmdout - read");
                cleanup();
                exit(1);
            }
        }
	return trd;
}

int
readv(const char *path, const char *restrict fmt, ...)
{
	FILE *f;
	int ret;
	va_list ap;

	if (!(f = fopen(path, "r")))
		return EOF;

	va_start(ap, fmt);
	ret = vfscanf(f, fmt, ap);
	va_end(ap);

	fclose(f);

	return ret;
}

pid_t
spawn(const char *const *arg)
{
	pid_t ret;
	switch (ret = fork()) {
	case -1:
		perror("spawn - fork");
		cleanup();
		exit(1);
	case 0:
		setsid();
		execvp(arg[0], (char *const *) arg);
		perror("spawn - child - execvp");
		_exit(127);
	default:
		return ret;
	}
}
