#include "blocks/battery.h"
#include "blocks/calendar.h"
#include "blocks/cputemp.h"
#include "blocks/cpuuse.h"
// #include "blocks/hotspot.h"
// #include "blocks/mail.h"
// #include "blocks/mem.h"
#include "blocks/wet.h"
// #include "blocks/nettraf.h"
// #include "blocks/volume.h"
#include "blocks/vol1.h"
#include "blocks/miniwin.h"
#include "blocks/ram.h"

/* NOSIGCHAR must be less than 32.
 * At max, NOSIGCHAR - 1 number of clickable blocks are allowed.
 * Raw characters larger than NOSIGCHAR and smaller than ' ' in ASCII
   character set can be used for signaling color change in status.
 * The character corresponding to NOSIGCHAR + 1 ('\x0b' when
   NOSIGCHAR is 10) will switch the active colorscheme to the first one
   defined in colors array in dwm's config.h and so on.
 * If you wish to change NOSIGCHAR, don't forget to update its value in
   dwm.c and color codes in util.h. */
// #define NOSIGCHAR                '\x20'

static const char rightdelimiter[] = { "" };
static const char leftdelimiter[] = { "" };

/* If interval of a block is set to 0, the block will only be updated once at
   startup.
 * If interval is set to a negative value, the block will never be updated in
   the main loop.
 * Set funcc to NULL if clickability is not required for the block.
 * Set signal to 0 if both clickability and signaling are not required for the
   block.
 * Signal must be less than NOSIGCHAR for clickable blocks.
 * If multiple signals are pending, then the lowest numbered one will be
   delivered first. */

/* funcu - function responsible for updating status text of the block
           (it should return the length of the text (including the terminating
            null byte), if the text was updated and 0 otherwise)
 * funcc - function responsible for handling clicks on the block */

/* 1 interval = INTERVALs seconds, INTERVALn nanoseconds */
#define INTERVALs                       2
#define INTERVALn                       0

static Block blocks[] = {
/*      funcu                   funcc                   interval        signal */
        { cpuuseu,              NULL,                   1,              1 },
        { cputempu,             NULL,                   15,             2 },
        { miniwinu,             NULL,                   0,              11 },
        // { memu,                 NULL,                   8,              3 },
        { ramu,                 NULL,                   8,              3 },
        // { nettrafu,          NULL,                   1,              0 },
        { batteryu,             batteryc,               240,            4 },
        // { volumeu,           NULL,                   1200,           10 }, //PULSEAUDIO
        { volume1u,             NULL,                   1200,           10 },
        { wetu,                 NULL,                   900,            5 },
        { calendaru,            NULL,                   7,              6 },
};
