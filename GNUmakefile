PREFIX = /usr/local

CC = ccache gcc
CFLAGS = -pedantic -Wall -march=native -pipe -s -Os -flto -fpie -finline-functions -fomit-frame-pointer -fexceptions -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fstack-clash-protection -fcf-protection -Wno-incompatible-pointer-types

X11CFLAGS = $(shell pkg-config --cflags x11)
X11LIBS = $(shell pkg-config --libs x11)

BLOCKS := $(wildcard blocks/*.c)

all: dsblocks sigdsblocks/sigdsblocks xgetrootname/xgetrootname

dsblocks.o: dsblocks.c shared.h config.h siglookup.h
	${CC} -o $@ -c -Wno-missing-field-initializers -Wno-unused-parameter ${CFLAGS} ${X11CFLAGS} $<

siglookup.h: config.h
	@awk -f siglookup config.h > siglookup.h

util.o: util.c util.h shared.h
	${CC} -o $@ -c ${CFLAGS} ${X11CFLAGS} $<

blocks/%.o: blocks/%.c blocks/%.h util.h shared.h
	${CC} -o $@ -c -Wno-unused-parameter ${CFLAGS} $<

dsblocks: dsblocks.o util.o ${BLOCKS:c=o}
	${CC} -o $@ $^ ${X11LIBS}

sigdsblocks/sigdsblocks: sigdsblocks/sigdsblocks.c
	${CC} -o $@ ${CFLAGS} $<

xgetrootname/xgetrootname: xgetrootname/xgetrootname.c
	${CC} -o $@ ${CFLAGS} ${X11CFLAGS} $< ${X11LIBS}

clean:
	rm -f blocks/*.o *.o dsblocks siglookup.h sigdsblocks/sigdsblocks xgetrootname/xgetrootname

BINDIR = ${DESTDIR}${PREFIX}/bin

install: all
	mkdir -p ${BINDIR}
	cp -f dsblocks sigdsblocks/sigdsblocks xgetrootname/xgetrootname ${BINDIR}
	chmod 755 ${BINDIR}/dsblocks ${BINDIR}/sigdsblocks ${BINDIR}/xgetrootname

uninstall:
	rm -f ${BINDIR}/dsblocks ${BINDIR}/sigdsblocks ${BINDIR}/xgetrootname

.PHONY: all clean install uninstall
